angular.module('com.angular.coba.service', [ 'ngResource' ]);
angular.module('com.angular.coba.service').factory('getEmployeeListService', [ 
	'$resource', 
	function($resource) {
		return $resource('/angular-coba/employee/get.all', {}, {
			get : {
				method : 'GET'
			}
		});
	} 
]);