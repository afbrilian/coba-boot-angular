angular.module('com.angular.coba.controller', []);
angular.module('com.angular.coba.controller').controller('employeeController', [ 
	'$scope' , 
	'getEmployeeListService', 
	function($scope, getEmployeeListService){
		$scope.employees = [];
		$scope.headingTitle = "Employee List";
		$scope.getEmployees = function () {
			getEmployeeListService.get({}, function(response) {
				console.log("controller response");
				console.log(response);
				$scope.employees = response.content;
			}, function(response) {
				console.log("test");
			});
		}
	} 
]);