<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="com.angular.coba.app">
<head>
	<link rel="stylesheet" href="<c:url value='/webjars/bootstrap/3.3.6/css/bootstrap.css'/>">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Spring boot and Angularjs</title>
</head>
<body ng-controller="employeeController">
	<h2>Welcome</h2>
	<div class="home-section">
		<ul class="menu-list">
			<li><a href="#/employee" ng-click="getEmployees()">employee</a></li>
		</ul>
	</div>
	<table>
		<tr ng-repeat="item in employees">
			<td>Id : {{item.id}}</td>
			<td>Name : {{item.name}}</td>
			<td>Address : {{item.address}}</td>
		</tr>
	</table>
	<script src="<c:url value='/webjars/angularjs/1.4.9/angular.js'/>"></script> 
	<script src="<c:url value='/webjars/angularjs/1.4.9/angular-resource.js'/>"></script>
	<script src="<c:url value='/webjars/angularjs/1.4.9/angular-route.js'/>"></script>
	<script src="<c:url value='/static/js/app.js'/>"></script>
	<script src="<c:url value='/static/js/controller.js'/>"></script>
	<script src="<c:url value='/static/js/service.js'/>"></script>
</body>
</html>