package com.angular.coba.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.angular.coba.entity.Employee;
import com.angular.coba.service.EmployeeService;
import com.angular.coba.wrapper.ResponseListWrapper;
import com.angular.coba.wrapper.ResponseSingleWrapper;

@RestController
public class EmployeeApiController {

  private EmployeeService employeeService;

  @Autowired
  public EmployeeApiController(EmployeeService employeeService) {
    this.employeeService = employeeService;
  }

  @RequestMapping(value = "/employee/seed", method = RequestMethod.GET)
  public ResponseSingleWrapper<Employee> seedUser() {
    Employee employee = new Employee("Mark Drink Water", "London");
    employeeService.save(employee);
    return new ResponseSingleWrapper<Employee>(true, employee);
  }

  @RequestMapping(value = "/employee/save", method = RequestMethod.GET)
  public ResponseSingleWrapper<Employee> saveUser(@RequestParam String name,
      @RequestParam String address) {
    Employee employee = new Employee(name, address);
    employeeService.save(employee);
    return new ResponseSingleWrapper<Employee>(true, employee);
  }

  @RequestMapping(value = "/employee/get.all", method = RequestMethod.GET)
  public ResponseListWrapper<Employee> getAllEmployee() {
    return new ResponseListWrapper<Employee>(true, employeeService.getAllEmployee());
  }

}
