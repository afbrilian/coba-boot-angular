package com.angular.coba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.angular.coba.service.EmployeeService;

@Controller
public class MainController {

  private EmployeeService employeeService;

  @Autowired
  public MainController(EmployeeService employeeService) {
    this.employeeService = employeeService;
  }

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String homePage() {
    return "index";
  }

}
