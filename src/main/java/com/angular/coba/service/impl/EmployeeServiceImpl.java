package com.angular.coba.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.angular.coba.entity.Employee;
import com.angular.coba.repository.EmployeeRepository;
import com.angular.coba.service.EmployeeService;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

  private EmployeeRepository employeeRepository;

  @Autowired
  public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  @Override
  public void save(Employee employee) {
    employeeRepository.save(employee);
  }

  @Override
  public List<Employee> getAllEmployee() {
    return employeeRepository.findAll();
  }

}
