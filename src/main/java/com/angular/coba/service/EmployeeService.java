package com.angular.coba.service;

import java.util.List;

import com.angular.coba.entity.Employee;

public interface EmployeeService {
  void save(Employee employee);
  List<Employee> getAllEmployee();
}
