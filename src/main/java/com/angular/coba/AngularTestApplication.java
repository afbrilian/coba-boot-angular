package com.angular.coba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AngularTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularTestApplication.class, args);
	}
}
