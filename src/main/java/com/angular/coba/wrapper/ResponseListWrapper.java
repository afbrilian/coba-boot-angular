package com.angular.coba.wrapper;

import java.io.Serializable;
import java.util.List;

public class ResponseListWrapper<T> implements Serializable {

  private static final long serialVersionUID = 1204972512579846487L;

  private boolean success;
  private List<T> content;

  public ResponseListWrapper() {}

  public ResponseListWrapper(boolean success, List<T> content) {
    this.success = success;
    this.content = content;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public List<T> getContent() {
    return content;
  }

  public void setContent(List<T> content) {
    this.content = content;
  }

}
