package com.angular.coba.wrapper;

import java.io.Serializable;

public class ResponseSingleWrapper<T> implements Serializable {

  private static final long serialVersionUID = -5211293544437550581L;

  private boolean success;
  private T content;

  public ResponseSingleWrapper() {}

  public ResponseSingleWrapper(boolean success, T content) {
    this.success = success;
    this.content = content;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public T getContent() {
    return content;
  }

  public void setContent(T content) {
    this.content = content;
  }

}
